window.addEventListener('DOMContentLoaded', async () =>{

    const selectTag = document.getElementById('conference')

    const url = 'http://localhost:8000/api/conferences/'
    const response = await fetch(url)
    if (response.ok) {
        const data = await response.json()
        for (let conference of data.conferences) {
            const optionElement = document.createElement('option')
            optionElement.setAttribute('value', conference.href)
            optionElement.innerHTML = conference.name
            selectTag.appendChild(optionElement)
        }
        const divLoadingTag = document.getElementById('loading-conference-spinner')
        divLoadingTag.setAttribute('class', 'd-flex justify-content-center mb-3 d-none')
        selectTag.setAttribute('class', 'form-select')
    }

    const formTag = document.getElementById("create-attendee-form")
    formTag.addEventListener('submit', async event => {
        event.preventDefault()
        const attendeeObject = Object.fromEntries(new FormData(formTag))
        const json = JSON.stringify(attendeeObject)
        const url = `http://localhost:8001${attendeeObject.conference}attendees/`
        const fetchConfig = {
            method: 'POST',
            body: json,
            header: {'Content-type': 'application/json'}
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            formTag.reset()
            const checkAttendee = await response.json()
            console.log(checkAttendee)
        }

    })
})
