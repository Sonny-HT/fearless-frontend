const payloadCookie = await cookieStore.get('jwt_access_payload')

if (payloadCookie) {
    const decodedPayload = atob(payloadCookie.value)
    const payload = JSON.parse(decodedPayload)
    console.log(payload)
    if (payload.user.perms.includes('events.add_conference')) {
        const navLinkTag = document.getElementById('conference-nav')
        navLinkTag.setAttribute('class', 'nav-link')
    }

    if (payload.user.perms.includes("events.add_location")){
        const navLinkTag = document.getElementById('location-nav')
        navLinkTag.setAttribute('class', 'nav-link')
    }

}
