function createCard(name, description, pictureUrl, starts, ends, location) {
    return `
    <div class="col-md-4 mb-4">
        <div class="card" style=box-shadow: 6px 5px 10px #ccc;">
        <img src="${pictureUrl}" class="card-img-top" style="max-height:300px">
        <div class="card-body">
            <h5 class="card-title">${name}</h5>
            <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
            <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
            <h6 class="card-dates"> ${starts} - ${ends}</h6>
        </div>
        </div>
    </div>
    `;
}


window.addEventListener('DOMContentLoaded', async () => {
    const url = "http://localhost:8000/api/conferences/"
    try {
        const response = await fetch(url)
        if (!response.ok) {
            throw new Error('Response not ok')
        } else {
            const data = await response.json()

            for (let conference of data.conferences){
                const detailUrl = `http://localhost:8000${conference.href}`
                const detailResponse = await fetch(detailUrl)

                if (detailResponse.ok) {
                    const details = await detailResponse.json()
                    const title = details.conference.name
                    const description = details.conference.description
                    const pictureUrl = details.conference.location.picture_url
                    const starts = new Date(details.conference.starts).toLocaleDateString("en-US")
                    const ends = new Date(details.conference.ends).toLocaleDateString("en-US")
                    const location = details.conference.location.name
                    const html = createCard(title, description, pictureUrl, starts, ends, location)
                    const row = document.querySelector('.row')
                    row.innerHTML += html;
                }

            }
        }
    } catch (error) {
        console.error('error', error)
    }

});
