window.addEventListener('DOMContentLoaded', async () => {
    const url = "http://localhost:8000/api/locations/"
    try {
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()

            const dropDownTag = document.getElementById('location')
            for (let loc of data.locations){
               let locationElement = document.createElement('option')
               locationElement.setAttribute('value', loc.id)
               locationElement.innerHTML = loc.name
               dropDownTag.appendChild(locationElement)
            }


            const formTag = document.getElementById("create-conference-form")
            formTag.addEventListener('submit', async event => {
                event.preventDefault()
                const formData = new FormData(formTag)
                const json = JSON.stringify(Object.fromEntries(formData))
                
                const conferenceUrl = "http://localhost:8000/api/conferences/"
                const fetchConfig = {
                    method: 'POST',
                    body: json,
                    header: {
                        'Content-type': 'application/json',
                    }
                };
                const response = await fetch(conferenceUrl, fetchConfig)
                if (response.ok) {
                    formTag.reset()
                    const newLocation = await response.json()
                    console.log(newLocation)
                }
            })
        } else {
            throw new Error('API Response not ok')
        }
    } catch (error) {console.error(error, 'error')}
})
