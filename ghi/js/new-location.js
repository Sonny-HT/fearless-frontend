window.addEventListener('DOMContentLoaded', async () => {
    const stateUrl = "http://localhost:8000/api/states/"
    try {
        const response = await fetch(stateUrl)
        if (response.ok) {
            const data = await response.json()
            let stateFormSelect = document.getElementById("state")

            for (let state of data.states) {
                let optionElement = document.createElement('option')
                optionElement.setAttribute('value', state.abbreviation)
                optionElement.innerHTML = state.name
                stateFormSelect.appendChild(optionElement)
            }

            const formTag = document.getElementById("create-location-form")
            formTag.addEventListener('submit', async event => {
                event.preventDefault()
                const formData = new FormData(formTag);
                const json = JSON.stringify(Object.fromEntries(formData))
                const locationUrl = 'http://localhost:8000/api/locations/'
                const fetchConfig = {
                    method: "post",
                    body: json,
                    headers: {
                        'Content-Type': 'application/json',
                    }
                };

                const response = await fetch(locationUrl, fetchConfig);
                if (response.ok){
                    formTag.reset()
                    const newLocation = await response.json()
                    console.log(newLocation)
                }
            })


        } else {
            throw new Error('Response not ok')
        }
    } catch (error) {console.error('error', error)}
})
