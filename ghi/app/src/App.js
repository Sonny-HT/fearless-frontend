import Nav from "./compenents/Nav";
import AttendeesList from "./compenents/AttendeesList";
import LocationForm from "./compenents/LocationForm";
import ConferenceForm from "./compenents/ConferenceForm";
import AttendForm from "./compenents/AttendForm";
import PresentationForm from "./compenents/PresentationForm";
import MainPage from "./compenents/MainPage";
import React from "react";
import {BrowserRouter, Routes, Route} from 'react-router-dom';


function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  return (
    <BrowserRouter>
      <Nav/>
        <Routes>
          <Route index element={<MainPage />}/>
          <Route path='locations'>
            <Route path="new" element={<LocationForm/>}/>
          </Route>
          <Route path='conferences'>
            <Route path='new' element={<ConferenceForm/>}/>
            <Route path='attend' element={<AttendForm/>}/>
          </Route>
          <Route path='presentations'>
            <Route path='new' element={<PresentationForm/>}></Route>
          </Route>
          {/* <Route path='attendees' element={<AttendeesList/>}>

          </Route> */}

        </Routes>

      {/* <AttendeesList attendees={props.attendees}/> */}
    </BrowserRouter>


  );
}

export default App;
