import React, {useEffect, useState} from 'react';

function LocationForm() {
    const [states, setStates] = useState([]);
    const [name, setName] = useState('');
    const [roomCount, setRoomCount] = useState('')
    const [city, setCity] = useState('')
    const [state, setState] = useState('')
    const fetchData = async () => {
        const url = 'http://localhost:8000/api/states/'

        const response = await fetch(url)

        if (response.ok){
            const data = await response.json()
            setStates(data.states)
        } else {
            console.error('Error with api response')
        }
    }
    useEffect(()=> {fetchData()}, [])

    const handleSubmit = async event => {
        event.preventDefault()
        const data = {}
        data.room_count = roomCount
        data.name = name
        data.city = city
        data.state = state
        const locationUrl = 'http://localhost:8000/api/locations/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
              'Content-Type': 'application/json',
            },
        };
        const response = await fetch(locationUrl, fetchConfig)
        if (response.ok){
            console.log(await response.json())
            setName('');
            setRoomCount('');
            setCity('');
            setState('');
        } else {
            console.error('there was an error with the submission')
        }
    }


    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
            <h1>Create a new location</h1>
            <form id="create-location-form" onSubmit={handleSubmit}>
                <div className="form-floating mb-3">
                <input placeholder="Name" required type="text" name="name" id="name" className="form-control" onChange={event => setName(event.target.value)} value={name}></input>
                <label htmlFor="name">Name</label>
                </div>
                <div className="form-floating mb-3">
                <input placeholder="Room count" required type="number" name="room_count" id="room_count" className="form-control" onChange={event => setRoomCount(event.target.value)} value={roomCount}></input>
                <label htmlFor="room_count">Room count</label>
                </div>
                <div className="form-floating mb-3">
                <input placeholder="City" required type="text" name="city" id="city" className="form-control" onChange={event => setCity(event.target.value)} value={city}></input>
                <label htmlFor="city">City</label>
                </div>
                <div className="mb-3">
                <select required name="state" id="state" className="form-select" onChange={event => setState(event.target.value)} value={state}>
                    <option value="">Choose a state</option>
                    {states.map(state => {
                        return (
                        <option value={state.abbreviation} key={state.abbreviation}>
                            {state.name}
                        </option>
                        );
                    })}
                </select>
                </div>
                <button className="btn btn-primary">Create</button>
            </form>
            </div>
        </div>
        </div>
    )
}

export default LocationForm
