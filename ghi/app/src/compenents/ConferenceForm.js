import React, {useState, useEffect} from 'react'

function ConferenceForm() {
    const [name, setName] = useState('')
    const [starts, setStarts] = useState('')
    const [ends, setEnds] = useState('')
    const [description, setDescription] = useState('')
    const [maxPresentations, setMaxPresentation] = useState(0)
    const [maxAttendees, setMaxAttendees] = useState(0)
    const [location, setLocation] = useState('')



    const [locations, setLocations] = useState([])



    const fetchData = async () => {
        const url = 'http://localhost:8000/api/locations/'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setLocations(data.locations)
        }
    }

    useEffect (() => {fetchData()}, [])

    const handleSubmit = async event => {
        event.preventDefault()
        const data = {}
        data.name = name
        data.starts = starts
        data.ends = ends
        data.description = description
        data.max_presentations = maxPresentations
        data.max_attendees = maxAttendees
        data.location = location
        const conferenceUrl = 'http://localhost:8000/api/conferences/'
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(data),
            headers: {
              'Content-Type': 'application/json',
            },
        }
        const response = await fetch (conferenceUrl, fetchConfig)
        if (response.ok) {
            console.log(await response.json())
            setName('')
            setStarts('')
            setEnds('')
            setDescription('')
            setMaxPresentation('')
            setMaxAttendees('')
            setLocation('')
        } else {
            console.error('there was an error with the submission')
        }
    }



    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Conference</h1>
            <form id="create-conference-form" onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input placeholder="Name" required type="text" id="name" name="name" className="form-control" onChange={event => setName(event.target.value)} value={name}></input>
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Starts" required type="date"  id="starts" name="starts" className="form-control" onChange={event => setStarts(event.target.value)} value={starts}></input>
                <label htmlFor="starts">Starts</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Ends" required type="date"  id="ends" name="ends" className="form-control" onChange={event => setEnds(event.target.value)} value={ends}></input>
                <label htmlFor="ends">Ends</label>
              </div>
              <div className="form-floating mb-3">
                <textarea className="form-control" id="description" name="description" rows="6" onChange={event => setDescription(event.target.value)} value={description}></textarea>
                <label htmlFor="description">Description</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="max_presentations" required type="number"  id="max_presentations" name="max_presentations" className="form-control" onChange={event => setMaxPresentation(event.target.value)} value={maxPresentations}></input>
                <label htmlFor="max_presentations">Max Presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="max_attendees" required type="number"  id="max_attendees" name="max_attendees" className="form-control" onChange={event => setMaxAttendees(event.target.value)} value={maxAttendees}></input>
                <label htmlFor="max_attendees">Max Attendees</label>
              </div>
              <div className="mb-3">
                <select required id="location" className="form-select" name="location" onChange={event => setLocation(event.target.value)} value={location}>
                  <option value="">Select a Location</option>
                  {locations.map(location => {
                    return (
                        <option value={location.id} key={location.id}>{location.name}</option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    )
}

export default ConferenceForm
