import React, {useState, useEffect} from "react";


function PresentationForm() {
    const [conferences, setConferences] = useState([])

    const [conference, setConference] = useState('')

    const [formData, setFormData] = useState({
        presenter_name:'',
        presenter_email:'',
        company_name:'',
        title: '',
        synopsis:'',
    })


    const fetchData = async () => {
        const response = await fetch('http://localhost:8000/api/conferences/')
        if (response.ok) {
            const data = await response.json()
            setConferences(data.conferences)
        }
    }

    useEffect(() => {fetchData()}, [])

    const handleChange = event => {
        setFormData({...formData, [event.target.name]: event.target.value})
    }

    const handleSubmit = async event => {
        event.preventDefault()
        const url = `http://localhost:8000${conference}presentations/`

        const fetchConfig = {
            method:'post',
            body: JSON.stringify(formData),
            headers: {
                'Content-Type': 'application/json',
            },
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            console.log(await response.json())
            setFormData({
                presenter_name:'',
                presenter_email:'',
                company_name:'',
                title: '',
                synopsis:''})
        }
    }


    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new Presentation</h1>
            <form id="create-presentation-form" onSubmit={handleSubmit}>
              <div className="form-floating mb-3">
                <input placeholder="Presenter name" required type="text" name="presenter_name" id="presenter_name" className="form-control" onChange={handleChange}></input>
                <label htmlFor="presenter_name">Presenter name</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Presenter email" required type="email" name="presenter_email" id="presenter_email" className="form-control" onChange={handleChange}></input>
                <label htmlFor="presenter_email">Presenter email</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Company name" type="text" name="company_name" id="company_name" className="form-control" onChange={handleChange}></input>
                <label htmlFor="company_name">Company name</label>
              </div>
              <div className="form-floating mb-3">
                <input placeholder="Title" required type="text" name="title" id="title" className="form-control" onChange={handleChange}></input>
                <label htmlFor="title">Title</label>
              </div>
              <div className="mb-3">
                <label htmlFor="synopsis">Synopsis</label>
                <textarea className="form-control" id="synopsis" rows="3" name="synopsis" onChange={handleChange}></textarea>
              </div>
              <div className="mb-3">
                <select required name="conference" id="conference" className="form-select" onChange={event => {setConference(event.target.value)}}>
                  <option value="">Choose a conference</option>
                  {conferences.map(conference => {
                    return (
                        <option value={conference.href} key={conference.href}>{conference.name}</option>
                    )
                  })}
                </select>
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
    </div>
    )
}

export default PresentationForm
