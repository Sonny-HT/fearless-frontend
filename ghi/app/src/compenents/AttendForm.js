import React, {useState, useEffect} from 'react'

function AttendForm() {
    const [conferences, setConferences] = useState([])
    const [fullName, setFullName] = useState('')
    const [email, setEmail] = useState('')
    const [conference, setConference] = useState('')



    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/'
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setConferences(data.conferences)
        }
    }

    let spinnerClasses = 'd-flex justify-content-center mb-3';
    let dropdownClasses = 'form-select d-none';
    if (conferences.length > 0) {
        spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
        dropdownClasses = 'form-select';
    }

    useEffect(() => {fetchData()}, [])

    const handleSubmit = async event => {
        event.preventDefault()
        const data = {}
        data.name = fullName
        data.conference = conference
        data.email = email
        const url = `http://localhost:8001${conference}attendees/`
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        }
        const response = await fetch (url, fetchConfig)
        if (response.ok) {
            console.log(await response.json())
            setFullName('')
            setEmail('')
            setConference('')
        } else {
            console.error('there was an error with the submission')
        }
    }



    return (
			<div className="my-5 container">
				<div className="row">
					<div className="col col-sm-auto">
					<img width="300" className="bg-white rounded shadow d-block mx-auto mb-4" src="/logo.svg"></img>
					</div>
					<div className="col">
					<div className="card shadow">
						<div className="card-body">
						<form id="create-attendee-form" onSubmit={handleSubmit}>
							<h1 className="card-title">It's Conference Time!</h1>
							<p className="mb-3">
							Please choose which conference
							you'd like to attend.
							</p>
              {

              }
							<div className={spinnerClasses} id="loading-conference-spinner">
							<div className="spinner-grow text-secondary" role="status">
								<span className="visually-hidden">Loading...</span>
							</div>
							</div>
							<div className="mb-3">
							<select name="conference" id="conference" className={dropdownClasses} required onChange={event => setConference(event.target.value)} value={conference}>
								<option value="">Choose a conference</option>
                {conferences.map(conference => {
                  return (
                    <option value={conference.href} key={conference.href}>{conference.name}</option>
                  )
                })}
							</select>
							</div>
							<p className="mb-3">
							Now, tell us about yourself.
							</p>
							<div className="row">
							<div className="col">
								<div className="form-floating mb-3">
								<input required placeholder="Your full name" type="text" id="name" name="name" className="form-control" onChange={event => setFullName(event.target.value)} value={fullName}></input>
								<label htmlFor="name">Your full name</label>
								</div>
							</div>
							<div className="col">
								<div className="form-floating mb-3">
								<input required placeholder="Your email address" type="email" id="email" name="email" className="form-control" onChange={event => setEmail(event.target.value)} value={email}></input>
								<label htmlFor="email">Your email address</label>
								</div>
							</div>
							</div>
							<button className="btn btn-lg btn-primary">I'm going!</button>
						</form>
						<div className="alert alert-success d-none mb-0" id="success-message">
							Congratulations! You're all signed up!
						</div>
						</div>
					</div>
					</div>
				</div>
			</div>
    )
}

export default AttendForm
